"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

from NinjaModel import NinjaModel

########################################################
##              MODELS (MISC)                         ## 
########################################################

"""
model_file = 'MISC/BUTTON01.NJ'
texture_file = 'MISC/BUTTON01.PVR'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/BUTTON02.NJ'
texture_file = 'MISC/BUTTON01.PVR'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/CLOUD01.NJ'
texture_file = 'MISC/CLOUD01.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/DEMO1.NJ'
texture_file = 'MISC/DEMO1.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/END000A_20SKY.NJ'
texture_file = 'MISC/END000A_20SKY.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/END000A_BIRDS.NJ'
texture_file = 'MISC/END000A_BIRDS.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/KIRI.NJ'
texture_file = 'MISC/KIRI.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'MISC/KUMO01_KUMO01_1.NJ'
nj = NinjaModel(model_file)
nj.parse()
nj.export()

model_file = 'MISC/KUMO01_KUMO02_1.NJ'
nj = NinjaModel(model_file)
nj.parse()
nj.export()

model_file = 'MISC/LEVEL01.NJ'
nj = NinjaModel(model_file)
nj.parse()
nj.export()

model_file = 'MISC/LEVEL02.NJ'
nj = NinjaModel(model_file)
nj.parse()
nj.export()
"""

########################################################
##              MODELS (OBJ)                          ## 
########################################################

model_file = 'OBJ/COCK_GPOT.NJ'
texture_file = 'OBJ/COCK_GPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/COCK_LPOT.NJ'
texture_file = 'OBJ/COCK_LPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/COCK_MPOT.NJ'
texture_file = 'OBJ/COCK_MPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/COCK.NJ'
texture_file = 'OBJ/COCK.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/EVIL.NJ'
texture_file = 'OBJ/EVIL.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/FISH.NJ'
texture_file = 'OBJ/FISH.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/GUN_JUNK.NJ'
texture_file = 'OBJ/GUN_JUNK.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/GUN.NJ'
texture_file = 'OBJ/GUN.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/HATO_GPOT.NJ'
texture_file = 'OBJ/HATO_GPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/HATO.NJ'
texture_file = 'OBJ/HATO.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/HOME_LEG.NJ'
texture_file = 'OBJ/HOME_LEG.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/HOME.NJ'
texture_file = 'OBJ/HOME.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/IKAL.NJ'
texture_file = 'OBJ/IKAL.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/MIS2.NJ'
texture_file = 'OBJ/MIS2.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/MIS.NJ'
texture_file = 'OBJ/MIS.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/NAIL.NJ'
texture_file = 'OBJ/NAIL.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/PAI2.NJ'
texture_file = 'OBJ/PAI2.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/PAI_GUN.NJ'
texture_file = 'OBJ/PAI_GUN.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/PAI.NJ'
texture_file = 'OBJ/PAI.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/PER.NJ'
texture_file = 'OBJ/PER.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/PIYO.NJ'
texture_file = 'OBJ/PIYO.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/POD2.NJ'
texture_file = 'OBJ/POD2.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/POD.NJ'
texture_file = 'OBJ/POD.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/RGA.NJ'
texture_file = 'OBJ/RGA.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SHIP_EPOT.NJ'
texture_file = 'OBJ/SHIP_EPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SHIP_GPOT.NJ'
texture_file = 'OBJ/SHIP_GPOT.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SHIP.NJ'
texture_file = 'OBJ/SHIP.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SHIP_OP1.NJ'
texture_file = 'OBJ/SHIP_OP1.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SHIP_OP2.NJ'
texture_file = 'OBJ/SHIP_OP2.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/SUCK.NJ'
texture_file = 'OBJ/SUCK.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'OBJ/TAMA.NJ'
texture_file = 'OBJ/TAMA.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

########################################################
##                   STAGE 01                         ## 
########################################################

model_file = 'STG01/BOY_BOMB1J.NJ'
texture_file = 'STG01/BOY_BOMB1J.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/BOY_BOMB1.NJ'
texture_file = 'STG01/BOY_BOMB1.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/BOY_BOMB2.NJ'
texture_file = 'STG01/BOY_BOMB2.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/BOY.NJ'
texture_file = 'STG01/BOY.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/BOY_SHIELD.NJ'
texture_file = 'STG01/BOY_SHIELD.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/BOY_SWORD.NJ'
texture_file = 'STG01/BOY_SWORD.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/TENSO.NJ'
texture_file = 'STG01/TENSO.PVM'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

model_file = 'STG01/UNKAI1.NJ'
texture_file = 'STG01/UNKAI.PVR'
nj = NinjaModel(model_file, texture_file)
nj.parse()
nj.export()

########################################################
##              MODELS (END)                          ## 
########################################################

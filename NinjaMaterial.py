"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

import struct

class NinjaMaterial:

	def __init__(self):
		self.name = ''
		self.index = -1
		self.tex_index = -1
		self.diffuse = { 
			'r' : 255, 
			'g' : 255, 
			'b' : 255, 
			'a' : 255 
		}
		return None
	
	def setIndex(self, index):
		self.index = index
		return None

	def setTexIndex(self, index):
		self.name = 'material_%03d' % index
		self.tex_index = index
		return None

	def setDiffuseColor(self, r, g, b, a):
		self.diffuse['r'] = r / 255.0
		self.diffuse['g'] = g / 255.0
		self.diffuse['b'] = b / 255.0
		self.diffuse['a'] = a / 255.0
		return None

	def clone(self):
		other = NinjaMaterial()
		other.tex_index = self.tex_index
		other.diffuse['r'] = self.diffuse['r']
		other.diffuse['g'] = self.diffuse['g']
		other.diffuse['b'] = self.diffuse['b']
		other.diffuse['a'] = self.diffuse['a']
		return other

	def __eq__ (self, other):
		if self.tex_index != other.tex_index:
			return False

		if self.diffuse['r'] == other.diffuse['r']:
			return False

		if self.diffuse['b'] == other.diffuse['b']:
			return False

		if self.diffuse['g'] == other.diffuse['g']:
			return False

		if self.diffuse['a'] == other.diffuse['a']:
			return False

		return True

	def createDmfEntry(self, file):

		# 0x00 Name
		name = self.name
		while len(name) < 0x20:
			name += '\0'
		bytes = name.encode()
		file.write(bytes)
		
		# 0x20 Flags
		file.write(struct.pack('hh', self.index, self.tex_index))
		file.write(struct.pack('HHHHHH', 0, 0, 0, 0, 1, 0))

		# 0x30 Flags
		file.write(struct.pack('HHHHHHf', 0, 0, 0, 0, 0, 0, 0.5))
		
		# 0x40 Diffuse 
		file.write(struct.pack('f', self.diffuse['r']))
		file.write(struct.pack('f', self.diffuse['g']))
		file.write(struct.pack('f', self.diffuse['b']))
		file.write(struct.pack('f', self.diffuse['a']))
		
		# 0x50 Emmisive
		file.write(struct.pack('ffff', 0.0, 0.0, 0.0, 0.0))

		# 0x60 Specular
		file.write(struct.pack('ffff', 0.0, 0.0, 0.0, 0.0))
		return None


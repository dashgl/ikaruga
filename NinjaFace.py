"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

import struct

class NinjaFace:

	def __init__(self):
		self.mat_index = -1
		self.index = ( 0, 0, 0 )
		self.color = (
			{ 'r' : 255, 'g' : 255, 'b' : 255, 'a' : 255 },
			{ 'r' : 255, 'g' : 255, 'b' : 255, 'a' : 255 },
			{ 'r' : 255, 'g' : 255, 'b' : 255, 'a' : 255 }
		)
		self.norm = (
			{ 'x' : 0.0, 'y' : 0.0, 'z' : 0.0 },
			{ 'x' : 0.0, 'y' : 0.0, 'z' : 0.0 },
			{ 'x' : 0.0, 'y' : 0.0, 'z' : 0.0 }
		)
		self.diffuse_uv = (
			{ 'u' : 0.0, 'v' : 0.0 },
			{ 'u' : 0.0, 'v' : 0.0 },
			{ 'u' : 0.0, 'v' : 0.0 }
		)
		return None
	
	def setMatIndex(self, index):
		self.mat_index = index
		return None

	def setIndexes(self, a, b, c):
		self.index = ( a, b, c )
		return None

	def setNormals(self, a, b, c):
		self.norm = ( a, b, c )
		return None
	
	def setColors(self, a, b, c):
		self.color = (a, b, c)
		return None

	def setDiffuseUv(self, a, b, c):
		self.diffuse_uv = ( a, b, c )
		return None

	def __repr__(self):
		return "Material Id: %d, Indices: %d %d %d" % (
			self.mat_index,
			self.index[0],
			self.index[1],
			self.index[2]
		)

	def createDmfEntry(self, file):

		#Material Index
		file.write(struct.pack('HH', self.mat_index, 0))
		
		#Index ABC
		file.write(struct.pack('I', self.index[0]))
		file.write(struct.pack('I', self.index[1]))
		file.write(struct.pack('I', self.index[2]))
		
		# Color A
		file.write(struct.pack('B', self.color[0]['r']))
		file.write(struct.pack('B', self.color[0]['g']))
		file.write(struct.pack('B', self.color[0]['b']))
		file.write(struct.pack('B', self.color[0]['a']))
		
		# Color B
		file.write(struct.pack('B', self.color[1]['r']))
		file.write(struct.pack('B', self.color[1]['g']))
		file.write(struct.pack('B', self.color[1]['b']))
		file.write(struct.pack('B', self.color[1]['a']))
		
		# Color C
		file.write(struct.pack('B', self.color[2]['r']))
		file.write(struct.pack('B', self.color[2]['g']))
		file.write(struct.pack('B', self.color[2]['b']))
		file.write(struct.pack('B', self.color[2]['a']))
		
		# Normal A
		file.write(struct.pack('f', self.norm[0]['x']))
		file.write(struct.pack('f', self.norm[0]['y']))
		file.write(struct.pack('f', self.norm[0]['z']))
		
		# Normal B
		file.write(struct.pack('f', self.norm[1]['x']))
		file.write(struct.pack('f', self.norm[1]['y']))
		file.write(struct.pack('f', self.norm[1]['z']))
		
		# Normal C
		file.write(struct.pack('f', self.norm[2]['x']))
		file.write(struct.pack('f', self.norm[2]['y']))
		file.write(struct.pack('f', self.norm[2]['z']))
		
		# Diffuse Map A
		file.write(struct.pack('f', self.diffuse_uv[0]['u']))
		file.write(struct.pack('f', self.diffuse_uv[0]['v']))
		
		# Diffuse Map B
		file.write(struct.pack('f', self.diffuse_uv[1]['u']))
		file.write(struct.pack('f', self.diffuse_uv[1]['v']))
		
		# Diffuse Map C
		file.write(struct.pack('f', self.diffuse_uv[2]['u']))
		file.write(struct.pack('f', self.diffuse_uv[2]['v']))

		return None


"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

import struct

class NinjaTexture:
	
	def __init__(self):
		self.index = -1
		self.name = ''
		self.gbix = -1
		self.width = -1
		self.height = -1
		self.data = -1
		self.raw = None
		
		#DMF Data
		self.pos = -1
		return None

	def setIndex(self, index):
		self.index = index
		return None
	
	def setName(self, name):
		self.name = name
		return None

	def setGlobalIndex(self, gbix):
		self.gbix = gbix
		return None

	def setWidth(self, width):
		self.width = width
		return None

	def setHeight(self, height):
		self.height = height
		return None

	def setData(self, data):
		self.data = data
		return None

	def setRaw(self, raw):
		self.raw = raw
		return None

	def createDmfEntry(self, file):
		
		name = self.name
		while len(name) < 0x20:
			name += '\0'
		bytes = name.encode()
		file.write(bytes)

		bytes = struct.pack('HH', self.index, 0)
		file.write(bytes)

		self.pos = file.tell()
		file.write(struct.pack('II', 0, len(self.data)))
		file.write(struct.pack('HH', self.width, self.height))
		file.write(struct.pack('HH', 1, 1))
		file.write(struct.pack('III', 0, 0, 0))
		return None

	def writeDmfTexture(self, file):
		offset = file.tell()
		file.seek(self.pos, 0)
		file.write(struct.pack('I', offset))
		file.seek(0, 2)
		file.write(self.data)
		while file.tell() % 0x10:
			file.write(struct.pack('B', 0))
		return None

"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

import math
import struct
from Mat4 import Mat4 

class NinjaBone: # {

	def __init__(self):
		self.name = "";
		self.index = -1
		self.parentIndex = -1
		self.local = Mat4() 
		self.world = Mat4()
		self.localRot = Mat4()
		self.worldRot = Mat4()

		self.parent = None
		self.children = []
		self.rotation = [ 0, 0, 0 ]
		self.position = [ 0, 0, 0 ]
		self.scale = [ 1, 1, 1]
		return None

	def getIndex(self):
		return self.index

	def getWorld(self):
		return self.world.mtx

	def getScale(self):
		return self.scale
	
	def getPosition(self):
		return self.position

	def getRotation(self):
		return self.rotation

	def getWorldRot(self):
		return self.worldRot.mtx

	def setName(self, num):
		self.index = num
		self.name = 'bone_%03d' % num
		return None

	def setParent(self, parent):
		self.parent = parent
		self.parentIndex = parent.getIndex()
		self.world.multiply(parent.getWorld())
		self.worldRot.multiply(parent.getWorldRot())
		return None

	def add(self, child):
		self.children.append(child)
		child.setParent(self)
		return None

	def setScale(self, vec3):
		self.scale = vec3
		self.local.scale(vec3)
		self.world.scale(vec3)
		return None

	def setRotation(self, vec3):
		
		rot = [ 
			vec3[0],
			vec3[1],
			vec3[2]
		]

		self.rotation = rot
		self.local.rotate(rot)
		self.world.rotate(rot)
		self.localRot.rotate(rot)
		self.worldRot.rotate(rot)
		return None

	def setPosition(self, vec3):
		self.position = vec3
		self.local.translate(vec3)
		self.world.translate(vec3)
		return None

	def apply(self, vec3):
		return self.world.apply(vec3)

	def applyNorm(self, vec3):
		return self.worldRot.apply(vec3)

	def createDmfEntry(self, file):
		# 0x00 Name
		name = self.name
		while len(name) < 0x20:
			name += '\0'
		bytes = name.encode()
		file.write(bytes)

		# 0x20 Index
		file.write(struct.pack('hh', self.index, self.parentIndex))
		file.write(struct.pack('III', 0, 0, 0))

		# 0x30 Local Matrix
		m = self.local.mtx
		file.write(struct.pack('ffff', m[0][0], m[0][1], m[0][2], m[0][3]))
		file.write(struct.pack('ffff', m[1][0], m[1][1], m[1][2], m[1][3]))
		file.write(struct.pack('ffff', m[2][0], m[2][1], m[2][2], m[2][3]))
		file.write(struct.pack('ffff', m[3][0], m[3][1], m[3][2], m[3][3]))
		return None



"""

 MIT License

 Copyright (c) 2020 Benjamin Collins (kion @ dashgl.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

"""

import struct

class NinjaVertex:

	def __init__(self):
		self.pos = {
			'x' : 0.0,
			'y' : 0.0,
			'z' : 0.0
		}

		self.norm = {
			'x' : 0.0,
			'y' : 0.0,
			'z' : 0.0
		}

		self.color = {
			'r' : 255,
			'g' : 255, 
			'b' : 255,
			'a' : 255
		}

		self.skin_weight = [ 0, 0, 0, 0 ]
		self.skin_index = [ 0, 0, 0, 0 ]
		return None
	
	def setPosition(self, x, y, z):
		self.pos['x'] = x
		self.pos['y'] = y
		self.pos['z'] = z
		return None

	def setNormal(self, x, y, z):
		self.norm['x'] = x
		self.norm['y'] = y
		self.norm['z'] = z
		return None

	def setColor(self, r, g, b, a):
		self.color['r'] = r
		self.color['g'] = g
		self.color['b'] = b
		self.color['a'] = a
		return None

	def setSkinWeight(self, n, index, weight):
		self.skin_weight[n] = weight;
		self.skin_index[n] = index
		return None

	def createDmfEntry(self, file):
		file.write(struct.pack('f', self.pos['x']))
		file.write(struct.pack('f', self.pos['y']))
		file.write(struct.pack('f', self.pos['z']))

		file.write(struct.pack('h', self.skin_index[0]))
		file.write(struct.pack('h', self.skin_index[1]))
		file.write(struct.pack('h', self.skin_index[2]))
		file.write(struct.pack('h', self.skin_index[3]))

		file.write(struct.pack('f', self.skin_weight[0]))
		file.write(struct.pack('f', self.skin_weight[1]))
		file.write(struct.pack('f', self.skin_weight[2]))
		file.write(struct.pack('f', self.skin_weight[3]))
		return None

	def __repr__(self):
		return "x: %.02f, y: %.02f, z: %.02f\n" % (self.pos['x'], self.pos['y'], self.pos['z'])

